# s_insect_outbreak.py
#!/usr/bin/env python3
""" Insect outbreak example modelling from chapter 3.7 in Nonlinear Dynamics
and Chaos by Steven H. Strogatz (second edition)"""

# Copyright (C) 2021 Mariane Yvonne Schneider
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Mariane Yvonne Schneider <myschneider@meiru.ch>
# Date: 08.10.2021

from scipy.integrate import solve_ivp
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
#import seaborn as sns
#sns.set_theme(font_scale=1.5)

# full formulation
# def populationdynamic(A, B, R, N, K):
#     pN = B * N / (A^2 + N^2)
#     dNdt = R * N * (1 - N / K) - pN
#
#     return dNdt

# dimensionless formulation
def populationdynamic(t, x, k, r):

    dxdt = r * x * (1 - x / k) - x**2 / (1 + x**2)

    return dxdt

k = 1
r = 1
x0 = [0, 0.5, 1]
tspan = [0, 50]
t = np.linspace(tspan[0], tspan[1], 200)
res = solve_ivp(populationdynamic, tspan, x0, args=(k, r), t_eval=t)

fig, ax = plt.subplots()
ax.plot(t, res.y[0, :], label='x0=0')
ax.plot(t, res.y[1, :], label='x0=0.5')
ax.plot(t, res.y[2, :], label='x0=1')
ax.legend()
ax.set_xlabel('time')
ax.set_ylabel('x')
plt.title('insect outbreak ODE solved for k=1, r=1')

# Reproduction of Figure 3.7.5.in Strogatz
xstabil = np.linspace(1.01, 40, 1000)
r = []
k = []
for ni in xstabil:
    _r = 2*ni**3/(1+ni**2)**2
    _k = 2*ni**3/(ni**2-1)
    r.append(_r)
    k.append(_k)

fig, ax = plt.subplots()
ax.set_xlabel('k')
ax.set_ylabel('r')
ax.plot(k, r)

# 3D
k = np.linspace(0.01, 40, 10)
r = np.linspace(0, 1, 90)
x0 = [1]
plt.style.use('seaborn-poster')

fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
result = []
K, R = np.meshgrid(k, r)
for ki, ri in zip(K.flatten(), R.flatten()):
    res = solve_ivp(populationdynamic, tspan, x0, args=(ki, ri), t_eval=t[[0, -1]])
    result.append(res.y.flatten()[-1])

result = np.asarray(result).reshape(*K.shape)
surf = ax.plot_surface(K, R, result, linewidth=0, cmap=cm.coolwarm)
ax.set_xlabel('k')
ax.set_ylabel('r')
ax.set_zlabel('x@50')
#fig.colorbar(surf, shrink=0.5, aspect=5)
fig.tight_layout()

fig, axs = plt.subplots(nrows=2, sharex='col')
mng = plt.get_current_fig_manager()
try:
    mng.resize(*mng.window.maxsize())
except AttributeError:
    # If there is no window, don't do anything
    pass


# Bifurcation in the derivative plot
ax = axs[0]
# Sample all relevant initial (or final, it's the same) conditions
x_ = np.linspace(0, 1.5, 1000)
for ri in np.linspace(0.5, 0.54, 10):
    ax.plot(x_, populationdynamic(0, x_, k=40, r=ri), label=f'r={ri:.3f}')
ax.axhline(0, linewidth=1, color='k')
ax.legend()
ax.set_xlabel('x')
ax.set_ylabel('dx/dt')
ax.grid()
ax.set_axisbelow(True)

# Bifurcation diagram without stability information
# as level-set 0 of populationdynamic seen as a function of x and r
ax = axs[1]
# Sample all relevant initial (or final, it's the same) conditions
# and all relevant parameter values
x_range = np.linspace(0, 1.4, 50)
r_range = np.linspace(0.5, 0.54, 80)
X, R = np.meshgrid(x_range, r_range)
F = populationdynamic(0, X, 40, R)
plt.contour(X, R, F, np.array([0]))
ax.set_ylabel('r')
ax.set_xlabel('x (fixed point, dx/dt = 0)')
ax.grid()
ax.set_axisbelow(True)

ax.set_xlim(-0.1, ax.get_xlim()[1])
fig.tight_layout()
plt.show()
