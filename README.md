# Batch reactor feature based regression

Scripts to model the biological wastewater treatment processes in a sequencing batch reactor (SBR).

## Authors

* **Mariane Schneider**

## License

This project is licensed under the GPLv3+ license - see the [LICENSE](LICENSE) file for details

## Repository structure
The folder structure of the repository is:

    .
    ├── data
    ├── doc
    └── julia

The `data` folder is meant to store raw data and the output of scripts.
It is actually a placeholder, no data should be added to the repository.
The data was used for the corresponding publication can be found here:
 [![DOI](https://zenodo.org/badge/DOI/)](https://doi.org/)


The `doc` folder contains documents used during the development of this package.
They do not contain actual documentation; to get documentation please refer to the [article](https://engrxiv.org/).

The `julia` folder contains scripts implementing the analysis of the data using the features from the [not yet defined](https://gitlab.com/) module.

## How to create the figures from the article?
