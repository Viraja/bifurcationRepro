# s_reproduce_bifurcation.py
#!/usr/bin/env python3
""" The scrips aims to reproduce the conference article Gomez-Acata, Rigel V.,
Pablo A. Lopez-Perez, Rafael Maya-Yescas, and Ricardo Aguilar-Lopez.
“Bifurcation Analysis of Continuous Aerobic Nonisothermal Bioreactor for
Wastewater Treatment.” In IFAC Proceedings Volumes, 45:24–29. Cancún, México:
Elsevier, 2012. https://doi.org/10.3182/20120620-3-MX-3012.00054.
."""

# Copyright (C) 2021 Mariane Yvonne Schneider
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Mariane Yvonne Schneider <myschneider@meiru.ch>
# Date: 23.09.2021

from pycont import Template, Contract

VOLUME = 15000  # m3
Q = 0.081       # m3 s-1
TAU = VOLUME/Q  # s

# State variables
SUBSTRATE =
COD =
BIOM =
OD =
TEMP =

# Additional terms
MASSTR =
KLA =
KD =
